-- COMANDO: psql -h localhost -U postgres -d ras_sp -f "exportAllTablesToCsv.sql"

-- TABELA DE AGENDAMENTO
COPY (SELECT * FROM ag67a) TO '/home/ljasmim/Área de Trabalho/NSWindows/DATABASE_SP/fusao/agendamento.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';

-- TABELA DE ATENDIMENTO COM DIAGNÓSTICO
COPY (SELECT * FROM aT54a) TO '/home/ljasmim/Área de Trabalho/NSWindows/DATABASE_SP/fusao/atendimento_diagnostico.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';

-- TABELA DE AGENDAMENTO COM PROCEDIMENTO
COPY (SELECT * FROM at54b) TO '/home/ljasmim/Área de Trabalho/NSWindows/DATABASE_SP/fusao/atendimento_procedimento.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
