-- NÚMERO DE OCORRÊNCIAS EM CADA TABELA
SELECT 'ag67a' AS tabela, count(id) FROM ag67a
WHERE datavaga LIKE '%/%' OR
	length(datavaga) <> 19 OR
      dataagendamento LIKE '%/%' OR
	length(dataagendamento) <> 19
UNION
SELECT 'at54a' AS tabela, count(id) FROM at54a
WHERE datacompleta LIKE '%/%' OR
	length(datacompleta) <> 19 
UNION
SELECT 'at54b' AS tabela, count(id) FROM at54b
WHERE datacompleta LIKE '%/%' OR
	length(datacompleta) <> 19