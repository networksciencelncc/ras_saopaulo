-- COMANDO LINUX: psql -h localhost -U postgres -d ras_sp -f "exportAnalyzePostgres2.sql"
-- COMANDO WINDOWS: psql -U postgres -d ras_sp -f "exportAnalyzePostgres2.sql"

-- LOCAL LINUX: '/home/ljasmim/Área de Trabalho/NSWindows/DATABASE_SP/005_analises/
-- LOCAL WINDOWS: 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\

/*
-- REGISTROS VÁLIDOS E INVÁLIDOS EM CADA TABELA - POR CNS
SELECT now();
COPY (SELECT t0.tabelas, t0.registros, t1.cns_invalido FROM
	(SELECT 'Agendamentos'::varchar AS tabelas, count(id) AS registros FROM ag67a ag
	UNION
	SELECT 'Atendimento com diagnóstico'::varchar AS tabelas, count(id) AS registros FROM at54a ata
	UNION
	SELECT 'Atendimento com Procedimento'::varchar AS tabelas, count(id) AS registros FROM at54b atb) AS t0
    LEFT JOIN
	(SELECT 'Agendamentos'::varchar as tabelas, count(*) AS cns_invalido FROM ag67a ag
	WHERE character_length(codigocns) <> 15
	UNION
	SELECT 'Atendimento com diagnóstico'::varchar as tabelas, count(*) AS cns_invalido FROM at54a ata
	WHERE character_length(codigocns) <> 15
	UNION
	SELECT 'Agendamentos com procedimento'::varchar as tabelas, count(*) AS cns_invalido FROM at54b atb
	WHERE character_length(codigocns) <> 15) as t1
	ON t0.tabelas = t1.tabelas)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\01_cns_validos_alldata.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- PACIENTES VÁLIDOS DISTINTOS EM TODAS TABELAS
SELECT now();
COPY (SELECT count(*) AS pacientes_distintos FROM
	((SELECT codigocns FROM ag67a 
	WHERE length(codigocns) = 15)
	UNION
	(SELECT codigocns FROM at54a
	WHERE length(codigocns) = 15)
	UNION
	(SELECT codigocns FROM at54b
	WHERE length(codigocns) = 15)) AS t0)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\02_cns_distintos_alldata.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE AGENDAMENTOS POR CNS
SELECT now();
COPY (SELECT codigocns, count(*) as agenda FROM ag67a 
		WHERE length(codigocns) = 15 
		GROUP BY codigocns
		ORDER BY agenda DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\03_freq_por_cns_ag67a.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE ATENDIMENTO COM DIAGNÓSTICO POR CNS
SELECT now();
COPY (SELECT codigocns, count(*) as atend_diag FROM at54a 
		WHERE length(codigocns) = 15 
		GROUP BY codigocns
		ORDER BY atend_diag DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\03_freq_por_cns_at54a.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE ATENDIMENTO COM PROCEDIMENTO POR CNS
SELECT now();
COPY (SELECT codigocns, count(*) as atend_proc FROM at54b
		WHERE length(codigocns) = 15 
		GROUP BY codigocns
		ORDER BY atend_proc DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\03_freq_por_cns_at54b.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA REGISTROS POR UNIDADES DE SAÚDE
SELECT now();
COPY (SELECT upper(t0.unidade) AS unidade, count(*) AS ocorrencia FROM
		(SELECT nomeestabelecimentosolicitante AS unidade FROM ag67a
		UNION ALL
		SELECT nomeestabelecimentoexecutante AS unidade FROM ag67a
		UNION ALL
		SELECT nomeestabelecimentoexecutante AS unidade FROM at54a
		UNION ALL
		SELECT nomeestabelecimentoexecutante AS unidade FROM at54b) AS t0
		GROUP BY unidade
		ORDER BY ocorrencia DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\04_freq_por_unidade_alldata.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA REGISTROS POR UNIDADES DE SAÚDE
SELECT now();
COPY (SELECT upper(t0.unidade) AS unidade, count(*) AS ocorrencia FROM
		(SELECT nomeestabelecimentosolicitante AS unidade FROM ag67a) AS t0
		GROUP BY unidade
		ORDER BY ocorrencia DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\04_freq_por_unidade_ag67a_sol.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA REGISTROS POR UNIDADES DE SAÚDE
SELECT now();
COPY (SELECT upper(t0.unidade) AS unidade, count(*) AS ocorrencia FROM
		(SELECT nomeestabelecimentoexecutante AS unidade FROM ag67a) AS t0
		GROUP BY unidade
		ORDER BY ocorrencia DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\04_freq_por_unidade_ag67a_exec.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA REGISTROS POR UNIDADES DE SAÚDE
SELECT now();
COPY (SELECT upper(t0.unidade) AS unidade, count(*) AS ocorrencia FROM
		(SELECT nomeestabelecimentoexecutante AS unidade FROM at54a) AS t0
		GROUP BY unidade
		ORDER BY ocorrencia DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\04_freq_por_unidade_at54a.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA REGISTROS POR UNIDADES DE SAÚDE
SELECT now();
COPY (SELECT upper(t0.unidade) AS unidade, count(*) AS ocorrencia FROM
		(SELECT nomeestabelecimentoexecutante AS unidade FROM at54b) AS t0
		GROUP BY unidade
		ORDER BY ocorrencia DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\04_freq_por_unidade_at54b.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA REGISTROS POR TIPO UNIDADES DE SAÚDE
SELECT now();
COPY (SELECT upper(t0.unidade) AS unidade, count(*) AS ocorrencia FROM
		(SELECT tipoestabelecimentosolicitante AS unidade FROM ag67a
		UNION ALL
		SELECT tipoestabelecimentoexecutante AS unidade FROM ag67a
		UNION ALL
		SELECT tipoestabelecimentoexecutante AS unidade FROM at54a
		UNION ALL
		SELECT tipoestabelecimentoexecutante AS unidade FROM at54b) AS t0
		GROUP BY unidade
		ORDER BY ocorrencia DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\04_freq_por_tipounidade_alldata.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

--FREQUENCIA DE ATENDIMENTOS POR PROFISSIONAIS
SELECT now();
COPY (SELECT * FROM
		(SELECT t0a.cbo AS profissional, t0a.count AS atend_diag, t1a.count AS atend_proc FROM
			(SELECT upper(t0.cbo) AS cbo, count(*) FROM
				(SELECT codigocboexecutante ||' - '|| nomecboexecutante AS cbo FROM at54a) AS t0
			GROUP BY cbo) AS t0a
		FULL JOIN
			(SELECT upper(t1.cbo) AS cbo, count(*) FROM
				(SELECT codigocboexecutante ||' - '|| nomecboexecutante AS cbo FROM at54b) AS t1
			GROUP BY cbo) AS t1a
		ON t0a.cbo = t1a.cbo
		ORDER BY atend_diag ASC) AS t2
	WHERE t2.profissional IS NOT NULL)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\05_freq_cbo_por_atend.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE DIAGNÓSTICOS POR ATENDIMENTOS
SELECT now();
COPY (SELECT t0.diag AS diagnostico, count(*) AS atendimentos FROM
		(SELECT upper(codigocid) || ' - ' || upper(nomecid) AS diag FROM at54a) AS t0
	GROUP BY diagnostico
	ORDER BY atendimentos DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\06_freq_diag_atend.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();
*/
-- FREQUENCIA DE PROCEDIMENTO POR ATENDIMENTOS
SELECT now();
COPY (SELECT t0.proc AS procedimento, count(*) AS atendimentos FROM
		(SELECT upper(codigoprocedimento) || ' - ' || upper(nomeprocedimento) AS proc FROM at54b) AS t0
	GROUP BY procedimento
	ORDER BY atendimentos DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\07_freq_proc_atend.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();