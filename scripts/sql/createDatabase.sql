-- COMANDO: psql -U postgres -f "C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\RASGRAPH\ras_saopaulo\dados\scripts\createDatabase.sql"

DROP DATABASE ras_sp;

CREATE DATABASE ras_sp
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Portuguese_Brazil.1252'
       LC_CTYPE = 'Portuguese_Brazil.1252'
       CONNECTION LIMIT = -1;
	   
\c ras_sp

DROP TABLE IF EXISTS regpaciente CASCADE;
CREATE TABLE regpaciente(
  id bigserial NOT NULL,
  cns character varying(255),
  idade character varying(255),
  sexo character varying(255),
  cep character varying(255),
  CONSTRAINT regpaciente_pkey PRIMARY KEY (id)
)WITH(OIDS=FALSE);
ALTER TABLE regpaciente OWNER TO postgres;
CREATE INDEX codCnsPacienteIdx ON regpaciente (cns);

DROP TABLE IF EXISTS ag67a CASCADE;
CREATE TABLE ag67a(
  id bigserial NOT NULL,
  dataVaga character varying(255),
  numeroDoMesDoAno character varying(255),
  h2NomeNivel2a character varying(255),
  h2NomeNivel3a character varying(255),
  h2NomeNivel4a character varying(255),
  h2NomeNivel5a character varying(255),
  tipoEstabelecimentoSolicitante character varying(255),
  nomeEstabelecimentoSolicitante character varying(255),
  cepSolicitante character varying(255),
  nomeEspecialidade character varying(255),
  codigoProcedimento character varying(255),
  nomeProcedimento character varying(255),
  h2NomeNivel2b character varying(255),
  h2NomeNivel3b character varying(255),
  h2NomeNivel4b character varying(255),
  h2NomeNivel5b character varying(255),
  tipoEstabelecimentoExecutante character varying(255),
  nomeEstabelecimentoExecutante character varying(255),
  cepExecutante character varying(255),
  nomeSituacaoAgendamento character varying(255),
  dataAgendamento character varying(255),
  numeroTotalDiasSolicitacao character varying(255),
  codigoCns character varying(255),
  quantidadeAgendamento character varying(255),
  CONSTRAINT ag67a_pkey PRIMARY KEY (id)
)WITH(OIDS=FALSE);
ALTER TABLE ag67a OWNER TO postgres;
CREATE INDEX codCnsAgendaIdx ON ag67a (codigoCns);

DROP TABLE IF EXISTS at54a CASCADE;
CREATE TABLE at54a(
  id bigserial NOT NULL,
  dataCompleta character varying(255),
  numeroDoAno character varying(255),
  numeroDoMesDoAno character varying(255),
  h2NomeNivel2a character varying(255),
  h2NomeNivel3a character varying(255),
  h2NomeNivel4a character varying(255),
  h2NomeNivel5a character varying(255),
  tipoEstabelecimentoExecutante character varying(255),
  nomeEstabelecimentoExecutante character varying(255),
  numeroCep character varying(255),
  codigoCboExecutante character varying(255),
  nomeCboExecutante character varying(255),
  nomeCid character varying(255),
  codigoCid character varying(255),
  codigoCns character varying(255),
  indicadorCidPrincipal character varying(255),
  quantidadeDiagnostico character varying(255),
  CONSTRAINT at54a_pkey PRIMARY KEY (id)
)WITH(OIDS=FALSE);
ALTER TABLE at54a OWNER TO postgres;
CREATE INDEX codCnsAtendDiagIdx ON at54a (codigoCns);

DROP TABLE IF EXISTS at54b CASCADE;
CREATE TABLE at54b(
  id bigserial NOT NULL,
  dataCompleta character varying(255),
  numeroDoAno character varying(255),
  numeroDoMesDoAno character varying(255),
  h2NomeNivel2a character varying(255),
  h2NomeNivel3a character varying(255),
  h2NomeNivel4a character varying(255),
  h2NomeNivel5a character varying(255),
  tipoEstabelecimentoExecutante character varying(255),
  nomeEstabelecimentoExecutante character varying(255),
  numeroCep character varying(255),
  codigoCboExecutante character varying(255),
  nomeCboExecutante character varying(255),
  nomeEspecialidade character varying (255),
  codigoProcedimento character varying(255),
  nomeProcedimento character varying(255),
  codigoCns character varying(255),
  cepCompleto character varying(255),
  quantidadeProcedimento character varying(255),
  CONSTRAINT at54b_pkey PRIMARY KEY (id)
)WITH(OIDS=FALSE);
ALTER TABLE at54b OWNER TO postgres;
CREATE INDEX codCnsAtendProcIdx ON at54b (codigoCns);
