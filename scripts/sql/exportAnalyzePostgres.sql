-- COMANDO LINUX: psql -h localhost -U postgres -d ras_sp -f "exportAnalyzePostgres.sql"
-- COMANDO WINDOWS: psql -U postgres -d ras_sp -f "C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\RASGRAPH\ras_saopaulo\scripts\sql\exportAnalyzePostgres.sql"

-- LOCAL LINUX: '/home/ljasmim/Área de Trabalho/NSWindows/DATABASE_SP/005_analises/
-- LOCAL WINDOWS: 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\
/*
-- NÚMERO DE OCORRÊNCIAS EM CADA TABELA
SELECT now();
COPY (SELECT t0.tabelas, t0.registros, t1.cns_invalido FROM
	(SELECT 'Agendamentos'::varchar AS tabelas, count(id) AS registros FROM ag67a ag
	UNION
	SELECT 'Atendimento com diagnóstico'::varchar AS tabelas, count(id) AS registros FROM at54a ata
	UNION
	SELECT 'Atendimento com Procedimento'::varchar AS tabelas, count(id) AS registros FROM at54b atb) AS t0
     LEFT JOIN
	(SELECT 'Agendamentos'::varchar as tabelas, count(*) AS cns_invalido FROM ag67a ag
	WHERE character_length(codigocns) <> 15
	UNION
	SELECT 'Atendimento com diagnóstico'::varchar as tabelas, count(*) AS cns_invalido FROM at54a ata
	WHERE character_length(codigocns) <> 15
	UNION
	SELECT 'Agendamentos com procedimento'::varchar as tabelas, count(*) AS cns_invalido FROM at54b atb
	WHERE character_length(codigocns) <> 15) as t1
	ON t0.tabelas = t1.tabelas)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\01_num_ocorr_validas_tabelas.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DAS OCORRÊNCIAS
SELECT now();
COPY (SELECT t2.datas, t2.agend, t2.atend_diag, t3.atb AS atend_proc FROM
	(SELECT t0.datas AS datas, t0.ag AS agend, t1.ata AS atend_diag FROM
		(SELECT (SUBSTRING(datavaga FROM 0 FOR 5) || '-' || numerodomesdoano) AS datas, count(*) AS ag FROM ag67a
		GROUP BY datas) AS t0
		LEFT JOIN
		(SELECT (numerodoano || '-' || numerodomesdoano) AS datas, count(*) AS ata FROM at54a
		GROUP BY datas) AS t1
		ON t0.datas = t1.datas) AS t2
	LEFT JOIN
	(SELECT (numerodoano || '-' || numerodomesdoano) AS datas, count(*) AS atb FROM at54b
	GROUP BY datas) AS t3
	ON t2.datas = t3.datas)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\02_freq_ocorr.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- RELAÇÃO DE AGENDAMENTOS ENTRE TIPOS DE UNIDADES
SELECT now();
COPY (SELECT t0.ag AS relacao_agendamento, count(*) FROM
	(SELECT '('||upper(tipoestabelecimentosolicitante)||
		') --> ('
		||upper(tipoestabelecimentoexecutante)||')' AS ag FROM ag67a) AS t0
	GROUP BY t0.ag
	ORDER BY count DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\03_rel_agenda_tipo_unid.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- RELAÇÃO DE AGENDAMENTOS ENTRE TIPOS DE UNIDADES - COM ESPECIALIDADE
SELECT now();
COPY (SELECT t0.ag AS relacao_agendamento, count(*) FROM
	(SELECT '('||upper(tipoestabelecimentosolicitante)||
		') --('||upper(nomeespecialidade)
		||')--> ('
		||upper(tipoestabelecimentoexecutante)||')' AS ag FROM ag67a) AS t0
	GROUP BY t0.ag
	ORDER BY count DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\04_rel_agenda_tipo_unid_espec.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE ATENDIMENTOS POR PROFISSIONAIS
SELECT now();
COPY (SELECT t0a.cbo AS profissional, t0a.count AS atend_diag, t1a.count AS atend_proc FROM
					(SELECT upper(t0.cbo) AS cbo, count(*) FROM
		 					(SELECT nomecboexecutante AS cbo FROM at54a) AS t0
	   			GROUP BY cbo) AS t0a
		 FULL JOIN
		 			(SELECT upper(t1.cbo) AS cbo, count(*) FROM
							(SELECT nomecboexecutante AS cbo FROM at54b) AS t1
					GROUP BY cbo) AS t1a
		 ON t0a.cbo = t1a.cbo
		 ORDER BY atend_diag DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\05_freq_cbo_por_atend.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE ATENDIMENTOS POR TIPO DE ESTABELECIMENTO
SELECT now();
COPY (SELECT t0a.tipo AS estabelecimento, t0a.count AS atend_diag, t1a.count AS atend_proc FROM
					(SELECT upper(t0.tipo) AS tipo, count(*) FROM
		 					(SELECT tipoestabelecimentoexecutante AS tipo FROM at54a) AS t0
	   			GROUP BY tipo) AS t0a
		 FULL JOIN
		 			(SELECT upper(t1.tipo) AS tipo, count(*) FROM
							(SELECT tipoestabelecimentoexecutante AS tipo FROM at54b) AS t1
					GROUP BY tipo) AS t1a
		 ON t0a.tipo = t1a.tipo
		 ORDER BY atend_diag DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\06_freq_tipo_unid_por_atend.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

--FREQUENCIA DE ATENDIMENTOS POR PROCEDIMENTO
SELECT now();
COPY (SELECT t0.proc AS procedimentos, count(*) FROM
					(SELECT upper(nomeprocedimento) AS proc FROM at54b) as t0
					GROUP BY t0.proc
					ORDER BY count DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises07_freq_atend_por_procedimento.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE ATENDIMENTOS POR CID
SELECT now();
COPY (SELECT t0.cid, count(*) FROM
					(SELECT upper(nomecid) AS cid FROM at54a) as t0
					GROUP BY t0.cid
					ORDER BY count DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\08_freq_atend_por_cid.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE ATENDIMENTOS POR GRUPO CID
SELECT now();
COPY (SELECT t0.grupo_cid, count(*) FROM
					(SELECT substring(upper(codigocid) FROM 0 FOR 3) AS grupo_cid FROM at54a) as t0
					GROUP BY t0.grupo_cid
					ORDER BY t0.grupo_cid ASC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\09_freq_atend_por_grupocid.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE ATENDIMENTOS POR INDICADOR DE CID PRINCIPAL
SELECT now();
COPY (SELECT t0.cid_principal, count(*) FROM
					(SELECT upper(indicadorcidprincipal) AS cid_principal FROM at54a) as t0
					GROUP BY t0.cid_principal
					ORDER BY t0.cid_principal ASC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\10_freq_atend_por_com_cid.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE REGISTROS POR CRS
SELECT now();
COPY (SELECT t2.crs, t2.agend, t2.atend_diag, t3a.atend_proc FROM
		(SELECT t0a.crs, t0a.agend, t1a.atend_diag FROM
			(SELECT t0.crs, count(*) AS agend FROM
				(SELECT upper(h2nomenivel2a) AS crs FROM ag67a) as t0
			GROUP BY t0.crs) AS t0a
	FULL JOIN
		(SELECT t1.crs, count(*) AS atend_diag FROM
			(SELECT upper(h2nomenivel2a) AS crs FROM at54a) as t1
		GROUP BY t1.crs) AS t1a
	ON t0a.crs = t1a.crs) AS t2
	FULL JOIN 
	(SELECT t3.crs, count(*) AS atend_proc FROM
		(SELECT upper(h2nomenivel2a) AS crs FROM at54b) as t3
	GROUP BY t3.crs) AS t3a
	ON t2.crs = t3a.crs
ORDER BY t2.crs)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\11_freq_reg_por_crs.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE REGISTROS POR STS
SELECT now();
COPY (SELECT t2.crs AS sts, t2.agend, t2.atend_diag, t3a.atend_proc FROM
		(SELECT t0a.crs, t0a.agend, t1a.atend_diag FROM
			(SELECT t0.crs, count(*) AS agend FROM
				(SELECT upper(h2nomenivel3a) AS crs FROM ag67a) as t0
			GROUP BY t0.crs) AS t0a
	FULL JOIN
		(SELECT t1.crs, count(*) AS atend_diag FROM
			(SELECT upper(h2nomenivel3a) AS crs FROM at54a) as t1
		GROUP BY t1.crs) AS t1a
	ON t0a.crs = t1a.crs) AS t2
	FULL JOIN 
	(SELECT t3.crs, count(*) AS atend_proc FROM
		(SELECT upper(h2nomenivel3a) AS crs FROM at54b) as t3
	GROUP BY t3.crs) AS t3a
	ON t2.crs = t3a.crs
ORDER BY t2.crs)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\12_freq_reg_por_sts.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE REGISTROS POR MACROREGIAO
SELECT now();
COPY (SELECT t2.crs, t2.agend, t2.atend_diag, t3a.atend_proc FROM
		(SELECT t0a.crs, t0a.agend, t1a.atend_diag FROM
			(SELECT t0.crs, count(*) AS agend FROM
				(SELECT upper(h2nomenivel4a) AS crs FROM ag67a) as t0
			GROUP BY t0.crs) AS t0a
	FULL JOIN
		(SELECT t1.crs, count(*) AS atend_diag FROM
			(SELECT upper(h2nomenivel4a) AS crs FROM at54a) as t1
		GROUP BY t1.crs) AS t1a
	ON t0a.crs = t1a.crs) AS t2
	FULL JOIN 
	(SELECT t3.crs, count(*) AS atend_proc FROM
		(SELECT upper(h2nomenivel4a) AS crs FROM at54b) as t3
	GROUP BY t3.crs) AS t3a
	ON t2.crs = t3a.crs
ORDER BY t2.crs)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\13_freq_reg_por_macro.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE REGISTROS POR MICROREGIAO
SELECT now();
COPY (SELECT t2.crs, t2.agend, t2.atend_diag, t3a.atend_proc FROM
		(SELECT t0a.crs, t0a.agend, t1a.atend_diag FROM
			(SELECT t0.crs, count(*) AS agend FROM
				(SELECT upper(h2nomenivel5a) AS crs FROM ag67a) as t0
			GROUP BY t0.crs) AS t0a
	FULL JOIN
		(SELECT t1.crs, count(*) AS atend_diag FROM
			(SELECT upper(h2nomenivel5a) AS crs FROM at54a) as t1
		GROUP BY t1.crs) AS t1a
	ON t0a.crs = t1a.crs) AS t2
	FULL JOIN 
	(SELECT t3.crs, count(*) AS atend_proc FROM
		(SELECT upper(h2nomenivel5a) AS crs FROM at54b) as t3
	GROUP BY t3.crs) AS t3a
	ON t2.crs = t3a.crs
ORDER BY t2.crs)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\14_freq_reg_por_micro.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE AGENDAMENTOS POR CNS VALIDO
SELECT now();
COPY (SELECT t0.cns, count(*) AS agendamentos FROM
		(SELECT codigocns AS cns FROM ag67a
		WHERE length(codigocns) = 15) AS t0
	GROUP BY t0.cns)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\15_freq_agend_cns.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE AGENDAMENTOS POR ESPECIALIDADES
SELECT now();
COPY (SELECT t0.espec AS especialidade, count(*) AS agendamentos FROM
		(SELECT upper(nomeespecialidade) AS espec FROM ag67a) AS t0
	GROUP BY especialidade
	ORDER BY agendamentos DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\16_freq_agend_espec.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE DIAGNÓSTICOS POR ATENDIMENTOS
SELECT now();
COPY (SELECT t0.diag AS diagnostico, count(*) AS atendimentos FROM
		(SELECT upper(nomecid) || ' (' || upper(codigocid) ||')' AS diag FROM at54a) AS t0
	GROUP BY diagnostico
	ORDER BY atendimentos DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\17_freq_diag_atend.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA DE PROCEDIMENTO POR ATENDIMENTOS
SELECT now();
COPY (SELECT t0.proc AS procedimento, count(*) AS atendimentos FROM
		(SELECT upper(nomeprocedimento) || ' (' || upper(codigoprocedimento) ||')' AS proc FROM at54b) AS t0
	GROUP BY procedimento
	ORDER BY atendimentos DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\18_freq_proc_atend.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();

-- FREQUENCIA OCORRÊNCIAS POR CNS
SELECT now();
COPY (SELECT t0.codigocns AS cns, count(*) AS ocorrencias FROM
		((SELECT codigocns FROM ag67a 
		WHERE length(codigocns) = 15)
		UNION ALL
		(SELECT codigocns FROM at54a
		WHERE length(codigocns) = 15)
		UNION ALL
		(SELECT codigocns FROM at54b
		WHERE length(codigocns) = 15)) AS t0
	GROUP BY cns
	ORDER BY ocorrencias DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\19_freq_ocorr_cns.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();
*/

-- FREQUENCIA OCORRÊNCIAS POR CAPITULO CID
SELECT now();
COPY (SELECT t0.capitulo AS capitulo, count(*) AS diagnosticos
		FROM((SELECT 'NÃO SE APLICA'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE '-%')
				UNION ALL
				(SELECT 'I - ALGUMAS DOENÇAS INFECCIOSAS E PARASITARIAS'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'A%' OR upper(codigocid) LIKE 'B%')
				UNION ALL
				(SELECT 'II - NEOPLASMAS'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'C%' OR upper(codigocid) LIKE 'D0%' OR upper(codigocid) LIKE 'D1%'
				OR upper(codigocid) LIKE 'D2%' OR upper(codigocid) LIKE 'D3%' OR upper(codigocid) LIKE 'D4%')
				UNION ALL
				(SELECT 'III - DOENÇAS DO SANGUE E DOS ORGÃOS HEMATOPOÊTICOS E ALGUNS TRANSTORNOS IMUNITARIOS'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'D5%' OR upper(codigocid) LIKE 'D6%' OR upper(codigocid) LIKE 'D7%'
				OR upper(codigocid) LIKE 'D8%')
				UNION ALL
				(SELECT 'IV - DOENÇAS ENDÓCRINAS, NUTRICIONAIS E METABÓLICAS'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'E%')
				UNION ALL
				(SELECT 'V - TRANSTORNOS MENTAIS E COMPORTAMENTAIS'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'F%')
				UNION ALL
				(SELECT 'VI - DOENÇAS DO SISTEMA NERVOSO'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'G%')
				UNION ALL
				(SELECT 'VII - DOENÇAS DO OLHO E ANEXOS'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'H0%' OR upper(codigocid) LIKE 'H1%' OR upper(codigocid) LIKE 'H2%' 
				OR upper(codigocid) LIKE 'H3%' OR upper(codigocid) LIKE 'H4%' OR upper(codigocid) LIKE 'H5%')
				UNION ALL
				(SELECT 'VIII - DOENÇAS DO OUVIDO E DA APOFISE MASTOIDE'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'H6%' OR upper(codigocid) LIKE 'H7%' 
				OR upper(codigocid) LIKE 'H8%' OR upper(codigocid) LIKE 'H9%')
				UNION ALL
				(SELECT 'IX - DOENÇAS DO APARELHO CIRCULATORIO'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'I%')
				UNION ALL
				(SELECT 'X - DOENÇAS DO APARELHO RESPIRATORIO'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'J%')
				UNION ALL
				(SELECT 'XI - DOENÇAS DO APARELHO DIGESTIVO'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'K%')
				UNION ALL
				(SELECT 'XII - DOENÇAS DA PELE E DO TECIDO SUBCUTÂNEO'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'L%')
				UNION ALL
				(SELECT 'XIII - DOENÇAS DO SISTEMA OSTEOMUSCULAR E DO TECIDO CONJUNTIVO'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'M%')
				UNION ALL
				(SELECT 'XIV - DOENÇAS DO APARELHO GENITURINARIO'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'N%')
				UNION ALL
				(SELECT 'XV - GRAVIDEZ, PARTO E PUERPÉRIO'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'O%')
				UNION ALL
				(SELECT 'XVI - ALGUMAS AFECÇÕES ORIGINADAS NO PERIODO PERINATAL'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'P%')
				UNION ALL
				(SELECT 'XVII - MALFORMÇÕES CONGÊNITAS, DEFORMIDADES E ANOMALIAS CROMOSSÔMICAS'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'Q%')
				UNION ALL
				(SELECT 'XVIII - SINTOMAS, SINAIS E ACHADOS ANORMAIS DE EXAMES CLINICOS E DE LABORATÓRIO, NÃO CLASSIFICADOS EM OUTRA PARTE'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'R%')
				UNION ALL
				(SELECT 'XIX - LESÕES, ENVENENAMENTOS E ALGUMAS OUTRAS CONSEQUÊNCIAS DE CAUSAS EXTERNAS'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'S%' OR upper(codigocid) LIKE 'T%')
				UNION ALL
				(SELECT 'XX - CAUSAS EXTERNAS DE MORBIDADE E DE MORTALIDADE'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'V%' OR upper(codigocid) LIKE 'W%' OR upper(codigocid) LIKE 'X%' OR upper(codigocid) LIKE 'Y%')
				UNION ALL
				(SELECT 'XXI - FATORES QUE INFLUENCIAM O ESTADO DE SAÚDE E O CONTATO COM OS SERVIÇOS DE SAÚDE'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'Z%')
				UNION ALL
				(SELECT 'XXII - CÓDIGO PARA PROPÓSITOS ESPECIAIS'::varchar(255) AS capitulo
				FROM at54a WHERE upper(codigocid) LIKE 'U%')) AS t0
		GROUP BY capitulo
		ORDER BY diagnosticos DESC)
TO 'C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\005_analises\20_freq_ocorr_cap_cid.csv' DELIMITER ';' CSV HEADER ENCODING 'UTF8';
SELECT now();
