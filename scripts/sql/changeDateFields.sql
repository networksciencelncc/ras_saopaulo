-- COMANDO LINUX: psql -h localhost -U postgres -d ras_sp -f "changeDateFields.sql"

-- COMANDO WINDOWS: psql -U postgres -d ras_sp -f "C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\RASGRAPH\ras_saopaulo\scripts\sql\changeDateFields.sql"

-- FREQUENCIA DAS OCORRÊNCIAS
--Ajustar Datas

BEGIN;

UPDATE ag67a SET datavaga = to_date(datavaga,'DD/MM/YYYY') || ' 00:00:00'
WHERE datavaga LIKE '%/%';

UPDATE ag67a SET dataagendamento = to_date(dataagendamento,'DD/MM/YYYY') || ' 00:00:00'
WHERE dataagendamento LIKE '%/%';

UPDATE at54a SET datacompleta = to_date(datacompleta,'DD/MM/YYYY') || ' 00:00:00'
WHERE datacompleta LIKE '%/%';

UPDATE at54b SET datacompleta = to_date(datacompleta,'DD/MM/YYYY') || ' 00:00:00'
WHERE datacompleta LIKE '%/%';

COMMIT;

