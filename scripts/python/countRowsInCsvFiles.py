import os
import csv

pathHomeLinux = "/home/ljasmim/Área de Trabalho/LJasmimWindows"
pathFileCSV = '/Workspaces/Projetos/LNCC/NetworkScience/DATABASE_SP/convertido'

arquivosCsv = (os.listdir(pathHomeLinux+pathFileCSV))

rowCountAG67a = 0
rowCountAT54a = 0
rowCountAT54b = 0
rowCountPaciente = 0

for x in arquivosCsv:
    
    prefix = x[:6]
    adress = pathHomeLinux + pathFileCSV + "/" + x
    
    with open(adress,"r") as f:
        reader = csv.reader(f, delimiter = ";", )
        count = sum(1 for row in reader) - 1
        print ("# Registros em " + x + " = " + str(count))
    
    if prefix == "AG-67A":
        rowCountAG67a += count
    elif prefix == "AT-54A":
        rowCountAT54a += count
    elif prefix == "AT-54B":
        rowCountAT54b += count
    else:
        rowCountPaciente = count
    
print ("AG-67A = " + str(rowCountAG67a))
print ("AT-54A = " + str(rowCountAT54a))
print ("AT-54B = " + str(rowCountAT54b))
print ("PACIENTE = " + str(rowCountPaciente))
    