# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 19:56:24 2017

GRÁFICOS GERADOS DA PRÉ-ANÁLISE REALIZADA NOS DADOS DA CIDADE DE
SÃO PAULO/SP
..
@author: ljasmim
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import pandas as pd

def millions(x, pos):
    'The two args are the value and tick position'
    return '%1.1f' % (x*1e-6)

path = 'C:\\Users\\leoja\\Workspaces\\Projetos\\LNCC\\NetworkScience\\DATABASE_SP\\005_analises\\'


#%% PLOT 01 - # OCORRÊNCIAS POR TABELA (CNS VÁLIDOS E INVÁLIDOS)

file_csv = '01_num_ocorr_validas_tabelas.csv'
file_png = '01_num_ocorr_validas_tabelas.png'

df = pd.read_csv(path+file_csv, sep=';')

# data to plot
n_groups = 3
registros = df['registros'].values
cns_invalidos = df['cns_invalido'].values
objects = df['tabelas'].values
 
# create plot bars
fig, ax = plt.subplots()
index = np.arange(n_groups)
bar_width = 0.35
opacity = 0.8
rects1 = plt.bar(index, registros, bar_width,
                 alpha=opacity,
                 color='g',
                 label='Registros Totais')
 
rects2 = plt.bar(index+bar_width, cns_invalidos, bar_width,
                 alpha=opacity,
                 color='r',
                 label='CNS_invalidos')

#labels
plt.title('Registros com Identificação do Paciente Válida', fontsize='medium')

plt.ylabel('# Registros (em milhões)', fontsize='small')
plt.yticks(fontsize='small')

plt.xticks(index, objects, fontsize='xx-small')

plt.legend(loc='upper left', fontsize='medium')
plt.style.use(['ggplot'])

#axis number format
majorLocator = tck.MultipleLocator(5000000)
majorFormatter = tck.FuncFormatter(millions)
minorLocator = tck.MultipleLocator(1000000)
ax.yaxis.set_major_locator(majorLocator)
ax.yaxis.set_major_formatter(majorFormatter)
ax.yaxis.set_minor_locator(minorLocator)

#Export .png
plt.savefig(path+file_png, dpi=300)

#%% PLOT 02 - FREQUENCIA DAS OCORRÊNCIAS

file_csv = '02_freq_ocorr.csv'
file_png = '02_freq_ocorr.png'

df = pd.read_csv(path+file_csv, sep=';')
df.datas = df.datas.apply(lambda x:x+'-01')

new_labels = ['mês','agendamentos','atendimento c/ diagnóstico','atendimento c/ procedimento']
df.columns = new_labels

# data to plot
t = df.mês.values
df['mês'] = np.array(t, dtype='datetime64[ns]')
df.index = df['mês']

cols = ['agendamentos','atendimento c/ diagnóstico','atendimento c/ procedimento']
df = df[cols]
s1 = df['agendamentos']
s2 = df['atendimento c/ diagnóstico']
s3 = df['atendimento c/ procedimento']

#create plot line
fig, ax = plt.subplots()
s1.plot(color='b', style='.-', legend=True)
s2.plot(color='r', style='.-', legend=True)
s3.plot(color='g', style='.-', legend=True)

#labels
plt.title('Frequência de Regitros dos Dados', fontsize='medium')
plt.ylabel('# Registros (em milhões)', fontsize='small')
plt.xlabel('')
plt.legend(loc='upper left', fontsize='small')
plt.style.use(['ggplot'])

#axis number format
plt.yscale('log')

#Export .png
plt.savefig(path+file_png, dpi=300)

#%% PLOT 03 - RELAÇÃO DE AGENDAMENTO POR TIPO DE UNIDADE

file_csv = '03_rel_agenda_tipo_unid.csv'
file_png = '03_rel_agenda_tipo_unid.png'

df = pd.read_csv(path+file_csv, sep=';', nrows)
#%%
