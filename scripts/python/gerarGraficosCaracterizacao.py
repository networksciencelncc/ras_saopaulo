# -*- coding: utf-8 -*-
"""
Created on Thu Mar  9 10:28:21 2017

CARACTERIZAÇÃO DE DADOS AMBULATORIAIS DA CIDADE DE SÃO PAULO/SP

@author: LeonardoJasmim
"""

import numpy as np
np.set_printoptions(threshold=np.nan)
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import pandas as pd

#%% FUNÇÕES PARA GERAÇÃO DOS GRÁFICOS
def removeCBO(l):
    for i in range(0,len(l)):
        l[i] = l[i][9:]
    return l

def millions(x, pos):
    'The two args are the value and tick position'
    return '%1.1f' % (x*1e-6)

def thousand(x, pos):
    'The two args are the value and tick position'
    return '%1.1f' % (x*1e-3)

def createBoxPlot(series, names, title, ylabel, path, fliers, whis):
    plt.figure()
    plt.grid(True,'both')
    plt.boxplot(series, showmeans=True, showfliers=fliers, whis=whis)

    plt.title(title, fontsize='medium')
    plt.ylabel(ylabel, fontsize='small')   
    intervalo = np.arange(1,len(series)+1) 
    plt.xticks(intervalo, names, fontsize='small')
    
    plt.style.use(['seaborn-whitegrid'])
    plt.savefig(path, dpi=400)
    plt.show()   
    
    
def createHBoxPlot(series, names, title, xlabel, text_size, path, fliers, whis):
    plt.figure()
    plt.grid(True,'both')
    plt.boxplot(series, showmeans=True, showfliers=fliers, whis=whis, vert=False, widths= (0.5,0.5))

    plt.title(title, fontsize=text_size)
    plt.xlabel(xlabel, fontsize=text_size)   
    intervalo = np.arange(1,len(series)+1) 
    print(intervalo)
    plt.yticks(intervalo, names, fontsize=text_size, rotation=90)
    
    plt.style.use(['seaborn-whitegrid'])
    plt.savefig(path, dpi=400)
    plt.show()  
    
def createHistograma2s(series, names, ylabel, xlabel, path, bins, isLog):
    fig, ax = plt.subplots()
    ax.hist(series[0], bins, ec='red', fc='none', lw=1.2, histtype='step', label=names[0])
    ax.hist(series[1], bins, ec='green', fc='none', lw=1.2, histtype='step', label=names[1])

    ax.legend(loc='upper right', fontsize='small')
    plt.ylabel(ylabel, fontsize='small') 
    plt.xlabel(xlabel, fontsize='small') 
    
    if(isLog):
        plt.yscale('log')
        plt.xscale('log')
    
    plt.style.use(['seaborn-whitegrid'])
    plt.savefig(path, dpi=400)
    plt.show()
    
def createHistograma3s(series, names, ylabel, xlabel, path, bins, isLog):
    fig, ax = plt.subplots()
    ax.hist(series[0], bins, ec='red', fc='none', lw=1.2, histtype='stepfilled', label=names[0])
    ax.hist(series[1], bins, ec='green', fc='none', lw=1.2, histtype='stepfilled', label=names[1])
    ax.hist(series[2], bins, ec='blue', fc='none', lw=1.2, histtype='stepfilled', label=names[2])

    ax.legend(loc='upper right', fontsize='small')
    plt.ylabel(ylabel, fontsize='small') 
    plt.xlabel(xlabel, fontsize='small') 
    if(isLog):
        plt.yscale('log')
        plt.xscale('log')
    
    plt.style.use(['seaborn-whitegrid'])
    plt.savefig(path, dpi=400)
    plt.show()
    
def createHistograma4s(series, names, ylabel, xlabel, path, bins, isLog):
    fig, ax = plt.subplots()
    ax.hist(series[0], bins, ec='red', fc='none', lw=1.2, histtype='step', label=names[0])
    ax.hist(series[1], bins, ec='green', fc='none', lw=1.2, histtype='step', label=names[1])
    ax.hist(series[2], bins, ec='blue', fc='none', lw=1.2, histtype='step', label=names[2])
    ax.hist(series[3], bins, ec='orange', fc='none', lw=1.2, histtype='step', label=names[3])

    ax.legend(loc='upper right', fontsize='small')
    plt.ylabel(ylabel, fontsize='small') 
    plt.xlabel(xlabel, fontsize='small') 
    
    if(isLog):
        plt.yscale('log')
        plt.xscale('log')
    
    plt.style.use(['seaborn-whitegrid'])
    plt.savefig(path, dpi=400)
    plt.show()
    
def createBarhPlot2s(objects, names, s1, s2, xlabel, path, text_size, legend_pos, off_set):
    # create plot bars
    n_groups = len(objects)
    fig, ax = plt.subplots()
    index = np.arange(n_groups)
    bar_width = 0.25
    half_bar = bar_width/2
    quarter_bar = half_bar/2
    opacity = 0.5
        
    plt.barh(index-bar_width, s1, bar_width, alpha=opacity,
                 color='g', label=names[0])
 
    plt.barh(index+bar_width, s2, bar_width, alpha=opacity,
                 color='r', label=names[1]) 
    
    for x in range(0, len(index)):
        plt.text(0, x-quarter_bar, ' ' + str(off_set+n_groups-x) + 'º ' + objects[x], fontsize=text_size, color='black')
    ax.set_yticklabels([])
    
    plt.xlabel(xlabel, fontsize='small')
    plt.xticks(fontsize='small')  
    
    majorFormatter = tck.FuncFormatter(millions)
    ax.xaxis.set_major_formatter(majorFormatter)

    plt.legend(loc=legend_pos, fontsize='medium')  
    plt.grid(True,axis='x')
    plt.style.use(['seaborn-whitegrid'])
    plt.savefig(path, dpi=400)
    plt.show()
    
def createBarhPlot(objects, s1, ylabel, xlabel, path, text_size, funcFormat, my_color):
    # create plot bars
    n_groups = len(objects)
    fig, ax = plt.subplots()
    index = np.arange(n_groups)
    bar_width = 0.5
    quarter_bar = bar_width/8
    opacity = 0.5
    
    plt.barh(index, s1, bar_width, alpha=opacity,
                 color=my_color, label=ylabel)
     
    for x in range(0, len(index)):
        plt.text(0, x-quarter_bar, ' ' + objects[x], fontsize=text_size)
    ax.set_yticklabels([])
    plt.xticks(fontsize='small')  
    
    plt.ylabel(ylabel, fontsize='medium')
    plt.xlabel(xlabel, fontsize='medium')
        
    majorFormatter = tck.FuncFormatter(funcFormat)
    ax.xaxis.set_major_formatter(majorFormatter)

    plt.grid(True,axis='x')
    plt.style.use(['seaborn-whitegrid'])
    plt.savefig(path, dpi=400)
    plt.show()
    
#%% PATHS
csv_path = 'C:\\Users\\leoja\\Workspaces\\Projetos\\LNCC\\NetworkScience\\DATABASE_SP\\005_analises\\'
png_path = 'C:\\Users\\leoja\\Workspaces\\Projetos\\LNCC\\NetworkScience\\RASGRAPH\\ras_saopaulo\\image\\'

#%% GRÁFICO 3 e 4 - FREQUENCIA DE REGISTROS VÁLIDOS POR DATASET - SEM OUTLIERS 
csv_file = '03_freq_por_cns_ag67a.csv'
ag67a = pd.read_csv(csv_path+csv_file, usecols=['agenda'],sep=';').values
csv_file = '03_freq_por_cns_at54a.csv'
at54a = pd.read_csv(csv_path+csv_file, usecols=['atend_diag'],sep=';').values
csv_file = '03_freq_por_cns_at54b.csv'
at54b = pd.read_csv(csv_path+csv_file, usecols=['atend_proc'],sep=';').values

#% BOXPLOT                   
createBoxPlot(
    series = [ag67a,at54a,at54b],                   
    names = ['Agendamentos','Atendimento c/ Diagnóstico', 'Atendimento c/ Procedimento'],
    title = '',
    ylabel = '# Registros por Paciente',
    path = png_path+'graf03_RegistroValidosPorPaciente.png',
    fliers = False,
    whis = 1.5
)

#% HISTOGRAMA
createHistograma3s(
    series = [ag67a,at54a,at54b], 
    xlabel = '# Registros (log)',
    ylabel = '# Pacientes (log)',                  
    names = ['Agendamentos','Atendimento c/ Diagnóstico', 'Atendimento c/ Procedimento'],
    path = png_path+'graf04_RegistroValidosPorPaciente.png',
    bins = 20,
    isLog = True
)

#%% GRAFICO 5 - FREQUENCIA DE REGISTRO POR UNIDADES DE SAÚDE
csv_file = '04_freq_por_unidade_ag67a_sol.csv'
s1 = pd.read_csv(csv_path+csv_file, usecols=['ocorrencia'],sep=';').values
csv_file = '04_freq_por_unidade_ag67a_exec.csv'
s2 = pd.read_csv(csv_path+csv_file, usecols=['ocorrencia'],sep=';').values
csv_file = '04_freq_por_unidade_at54a.csv'
s3 = pd.read_csv(csv_path+csv_file, usecols=['ocorrencia'],sep=';').values
csv_file = '04_freq_por_unidade_at54b.csv'
s4 = pd.read_csv(csv_path+csv_file, usecols=['ocorrencia'],sep=';').values
                
#% BOXPLOT                   
createBoxPlot(
    series = [s1,s2],                   
    names = ['Solicitante','Executante'],
    title = '',
    ylabel = '# Registros de Agendamento por Unidade',
    path = png_path+'graf04_RegistroAgendamentoPorUnidade.png',
    fliers = True,
    whis=3.0
)

#% BOXPLOT                   
createBoxPlot(
    series = [s3,s4],                   
    names = ['Atendimento c/ Diagnóstico', 'Atendimento c/ Procedimento'],
    title = '',
    ylabel = '# Registros de Atendimento por Unidade',
    path = png_path+'graf04_RegistroAtendimentoPorUnidade.png',
    fliers = True,
    whis=3.0
)

#% HISTOGRAMA
createHistograma2s(
    series = [s1,s2], 
    xlabel = '# Registros de Agendamento (log)',
    ylabel = '# Unidades (log)',                  
    names = ['Solicitante', 'Executante'],
    path = png_path+'graf04_HistogramaAgendamentoUnidades.png',
    bins='auto',
    isLog = True   
)

#% HISTOGRAMA
createHistograma2s(
    series = [s3,s4], 
    xlabel = '# Registros de Atendimento (log)',
    ylabel = '# Unidades (log)',                  
    names = ['c/ Diagnóstico', 'c/ Procedimento'],
    path = png_path+'graf04_HistogramaAtendimentoUnidades.png',
    bins=100,
    isLog = True   
)

#%% GRAFICO 5 - TOP 10 PROFISSIONAIS
csv_file = '05_freq_cbo_por_atend_2.csv'
df = pd.read_csv(csv_path+csv_file, nrows=10, sep=';')

ini_elem = 0;
fim_elem = 4;

# np.flipud - inverte a ordem do array
s1 = np.flipud(df['atend_diag'][ini_elem:fim_elem].values)
s2 = np.flipud(df['atend_proc'][ini_elem:fim_elem].values)
objects = np.flipud(removeCBO(df['profissional'][ini_elem:fim_elem].values))
names = ['Diagnóstico','Procedimento']
createBarhPlot2s(
        objects,
        names, 
        s1, 
        s2,
        '# Registros (em milhões)',
        path = png_path+'graf05_Top1-5_CBO.png',
        text_size = 10,
        legend_pos= 'lower right',
        off_set=0)

ini_elem = 5;
fim_elem = 9;

df = pd.read_csv(csv_path+csv_file, nrows=10, sep=';')
s1 = np.flipud(df['atend_diag'][ini_elem:fim_elem].values)
s2 = np.flipud(df['atend_proc'][ini_elem:fim_elem].values)
objects = np.flipud(removeCBO(df['profissional'][ini_elem:fim_elem].values))
names = ['Diagnóstico','Procedimento']

createBarhPlot2s(
        objects,
        names, 
        s1, 
        s2,
        '# Registros (em milhões)',
        path = png_path + 'graf05_Top6-10_CBO.png', 
        text_size = 9.5,
        legend_pos = 'lower right',
        off_set=4)


#%% GRAFICO 6 - TOP 10 DIAGNÓSTICOS e PROCEDIMENTOS

csv_file = '06_freq_diag_atend.csv'
df = pd.read_csv(csv_path+csv_file, nrows=12, sep=';')

s1 = df['atendimentos'][2:].values
objects = df['diagnostico'][2:].values

# BAR PLOT #01            
createBarhPlot(
        objects,
        s1,
        'Diagnósticos',
        '# Registros (em milhares)',
        path = png_path+'graf06_Top10_Diagnosticos.png',
        text_size = 7,
        funcFormat = thousand,
        my_color='r')

csv_file = '07_freq_proc_atend.csv'
df = pd.read_csv(csv_path+csv_file, nrows=10, sep=';')

s1 = df['atendimentos'][0:].values
objects = df['procedimento'][0:].values

# BAR PLOT #02
createBarhPlot(
        objects,
        s1,
        'Procedimentos',
        '# Registros (em milhões)',
        path = png_path+'graf07_Top10_Procedimentos.png',
        text_size = 5.8,
        funcFormat = millions,
        my_color='orange')

csv_file = '06_freq_diag_atend.csv'
diag = pd.read_csv(csv_path+csv_file, usecols=['atendimentos'],sep=';').values
csv_file = '07_freq_proc_atend.csv'
proc = pd.read_csv(csv_path+csv_file, usecols=['atendimentos'],sep=';').values

#% BOXPLOT                   
createHBoxPlot(
    series = [diag, proc],                   
    names = ['Diagnóstico','Procedimento'],
    title = '',
    xlabel = '# Registros',
    text_size='large',
    path = png_path+'graf08_Diagnostico_Procedimentos.png',
    fliers = False,
    whis = 3.0
)

#% HISTOGRAMA
createHistograma2s(
    series = [diag, proc], 
    xlabel = '# Registros de Atendimento (log)',
    ylabel = '# Diagnósticos e Procedimentos',                  
    names = ['Diagnóstico', 'Procedimento'],
    path = png_path+'graf09_HistogramaDiagProc.png',
    bins=50,
    isLog = True   
)

#%% GRAFICO 20 - TOP 10 DIAGNÓSTICOS POR CAPITULO CID

csv_file = '20_freq_ocorr_cap_cid.csv'
df = pd.read_csv(csv_path+csv_file, nrows=23, sep=';')

ini_elem = 0;
fim_elem = 23;

s1 = df['diagnosticos'][:].values
objects = df['capitulo'][:].values

s1 = np.flipud(df['diagnosticos'][ini_elem:fim_elem].values)
objects = np.flipud(df['capitulo'][ini_elem:fim_elem].values)
                      
                      
# BAR PLOT #01            
createBarhPlot(
        objects[0:11],
        s1[0:11],
        'Capítulo CID-10',
        '# Registros (milhares)',
        path = png_path+'graf20_CapituloCID_part1.png',
        text_size = 7,
        funcFormat = thousand,
        my_color='tomato')

# BAR PLOT #02           
createBarhPlot(
        objects[11:22],
        s1[11:22],
        'Capítulo CID-10',
        '# Registros (milhares)',
        path = png_path+'graf20_CapituloCID_part2.png',
        text_size = 7,
        funcFormat = thousand,
        my_color='tomato')