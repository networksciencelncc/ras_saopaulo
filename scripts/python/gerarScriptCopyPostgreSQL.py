#Gerar Script de importacao de dados CSV-> PostgreSql
import os

#Functions
def getCopyAgendamento(csvFile):
    comando = """COPY ag67a (dataVaga, numeroDoMesDoAno,h2NomeNivel2a,h2NomeNivel3a,h2NomeNivel4a,h2NomeNivel5a,tipoEstabelecimentoSolicitante,nomeEstabelecimentoSolicitante,cepSolicitante,nomeEspecialidade,codigoProcedimento,nomeProcedimento,h2NomeNivel2b,h2NomeNivel3b,h2NomeNivel4b,h2NomeNivel5b,tipoEstabelecimentoExecutante,nomeEstabelecimentoExecutante,cepExecutante,nomeSituacaoAgendamento,dataAgendamento,numeroTotalDiasSolicitacao,codigoCns,quantidadeAgendamento) FROM \'C:\\Users\\leoja\\Workspaces\\Projetos\\LNCC\\NetworkScience\\DATABASE_SP\\convertido\\"""+csvFile+"\' "+"using delimiters \';\' WITH NULL AS \'\' encoding \'utf8\' CSV HEADER;\n"""
    return comando

def getCopyAtendimentoDiagnostico(csvFile):
    comando = """COPY at54a (dataCompleta,numeroDoAno,numeroDoMesDoAno,h2NomeNivel2a,h2NomeNivel3a,h2NomeNivel4a,h2NomeNivel5a,tipoEstabelecimentoExecutante,nomeEstabelecimentoExecutante,numeroCep,codigoCboExecutante,nomeCboExecutante,nomeCid,codigoCid,codigoCns,indicadorCidPrincipal,quantidadeDiagnostico) FROM \'C:\\Users\\leoja\\Workspaces\\Projetos\\LNCC\\NetworkScience\\DATABASE_SP\\convertido\\"""+csvFile+"\' "+"using delimiters \';\' WITH NULL AS \'\' encoding \'utf8\' CSV HEADER;\n"""
    return comando

def getCopyAtendimentoProcedimento(csvFile):
    comando = """COPY at54b (dataCompleta,numeroDoAno,numeroDoMesDoAno,h2NomeNivel2a,h2NomeNivel3a,h2NomeNivel4a,h2NomeNivel5a,tipoEstabelecimentoExecutante,nomeEstabelecimentoExecutante,numeroCep,codigoCboExecutante,nomeCboExecutante,nomeEspecialidade,codigoProcedimento,nomeProcedimento,codigoCns,cepCompleto,quantidadeProcedimento) FROM \'C:\\Users\\leoja\\Workspaces\\Projetos\\LNCC\\NetworkScience\\DATABASE_SP\\convertido\\"""+csvFile+"\' "+"using delimiters \';\' WITH NULL AS \'\' encoding \'utf8\' CSV HEADER;\n"""
    return comando

def getCopyPaciente(csvFile):
    comando = """COPY regpaciente (cns, idade, sexo, cep) FROM \'C:\\Users\\leoja\\Workspaces\\Projetos\\LNCC\\NetworkScience\\DATABASE_SP\\convertido\\"""+csvFile+"\' "+"using delimiters \';\' WITH NULL AS \'\' encoding \'utf8\' CSV;\n"""
    return comando

def getCabecalho(nomeScript):
    return "-- COMANDO: psql -U postgres -d ras_sp -f \"C:\\Users\\leoja\\Workspaces\\Projetos\\LNCC\\NetworkScience\\RASGRAPH\\ras_saopaulo\\dados\\scripts\\"+nomeScript+"\"\n\nBEGIN;\n"

def anexarComando(prefix, comandosSql, x):
    if prefix == "AG-67A":
        comandosSql.append(getCopyAgendamento(x))
    elif prefix == "AT-54A":
        comandosSql.append(getCopyAtendimentoDiagnostico(x))
    elif prefix == "AT-54B":
        comandosSql.append(getCopyAtendimentoProcedimento(x))
    else:
        comandosSql.append(getCopyPaciente(x))

def createScript(comandos, path, nomeScript):
    script = open(path+nomeScript, 'w')
    script.write(getCabecalho(nomeScript)) 
    script.writelines(comandos)
    script.write("COMMIT;\nVACUUM;")
    script.close()
    return 1

#Main
pathWindows = '/Users/leoja'
pathCSV = '/Workspaces/Projetos/LNCC/NetworkScience/DATABASE_SP/convertido'
pathScript = '/Workspaces/Projetos/LNCC/NetworkScience/RASGRAPH/ras_saopaulo/dados/scripts/'

comandosSqlRegiaoNorte = []
comandosSqlRegiaoSul = []
comandosSqlRegiaoLeste = []
comandosSqlRegiaoCentroOeste = []
comandosSqlRegiaoSudeste = []
comandosSqlPaciente = []

# Montar Comandos
arquivosCsv = (os.listdir(pathWindows + pathCSV))

for x in arquivosCsv:
    prefix = x[:6]
    regiao = x[7:9]
    
    if regiao == "NT":
        anexarComando(prefix,comandosSqlRegiaoNorte,x)
    elif regiao == "SU":
        anexarComando(prefix,comandosSqlRegiaoSul,x)
    elif regiao == "SE":
        anexarComando(prefix,comandosSqlRegiaoSudeste,x)
    elif regiao == "LE":
        anexarComando(prefix,comandosSqlRegiaoLeste,x)
    elif regiao == "CO":
        anexarComando(prefix,comandosSqlRegiaoCentroOeste,x)
    else:
        anexarComando(prefix,comandosSqlPaciente,x)
       
# Gerar .CSV
if createScript(comandosSqlRegiaoNorte,pathWindows+pathScript, "copyFromCsvNT.sql"):
    print ('Script da Regiao Norte criado com sucesso!')
if createScript(comandosSqlRegiaoSul,pathWindows+pathScript, "copyFromCsvSU.sql"):
    print ('Script da Regiao Sul criado com sucesso!')
if createScript(comandosSqlRegiaoSudeste,pathWindows+pathScript, "copyFromCsvSE.sql"):
    print ('Script da Regiao Sudeste criado com sucesso!')
if createScript(comandosSqlRegiaoLeste,pathWindows+pathScript, "copyFromCsvLE.sql"):
    print ('Script da Regiao Leste criado com sucesso!')
if createScript(comandosSqlRegiaoCentroOeste,pathWindows+pathScript, "copyFromCsvCO.sql"):
    print ('Script da Regiao Centro-Oeste criado com sucesso!')
if createScript(comandosSqlPaciente,pathWindows+pathScript, "copyFromCsvPaciente.sql"):
    print ('Script dos Pacientes criado com sucesso!')
    
    
    
    
    
