$datapath = "C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\002_revisado\remessa_201704\"
$convpath = "C:\Users\leoja\Workspaces\Projetos\LNCC\NetworkScience\DATABASE_SP\003_convertido\remessa_201704\"

$files = Get-ChildItem -Filter *.csv -Force $datapath -Recurse

foreach ($file in $files){
	$newfile = $convpath + $file.name
	Get-Content $file.FullName | ForEach-Object {$_ -replace "\t",";"} | Out-File $newfile -Encoding 'UTF8'
	$message = $file.name + " convertido com sucesso!"
	echo $message
}

echo "Conversão concluída"